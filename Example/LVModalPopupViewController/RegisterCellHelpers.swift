//
//  RegisterCellHelpers.swift
//  LVModalPopupViewController_Example
//
//  Created by Victor Lisnic on 1/11/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit

protocol ReusableView: class {
    static var defaultReuseIdentifier: String { get }
}

extension ReusableView where Self: UIView {
    static var defaultReuseIdentifier: String {
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
}

protocol NibLoadableView : class {
    static var defaultNibName: String { get }
}

extension NibLoadableView where Self : UIView {
    static var defaultNibName: String {
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
}

extension UICollectionView {
    func register<T:UICollectionViewCell>(_:T.Type) where T : ReusableView {
        register(T.self, forCellWithReuseIdentifier: T.defaultReuseIdentifier)
    }

    func register<T:UICollectionViewCell>(_:T.Type) where T : ReusableView, T : NibLoadableView {
        let nib = UINib(nibName: T.defaultNibName, bundle: nil)
        register(nib, forCellWithReuseIdentifier: T.defaultReuseIdentifier)
    }

    func dequeueReusableCell<T:UICollectionViewCell>(for indexPath:IndexPath) -> T where T : ReusableView {
        guard let cell = dequeueReusableCell(withReuseIdentifier: T.defaultReuseIdentifier, for: indexPath) as? T  else {
            fatalError("No cell for deque identifier : \(T.defaultReuseIdentifier)")
        }
        return cell
    }
}

extension UITableView {
    func register<T:UITableViewCell>(_:T.Type) where T : ReusableView {
        register(T.self, forCellReuseIdentifier: T.defaultReuseIdentifier)
    }

    func register<T:UITableViewCell>(_:T.Type) where T : ReusableView, T : NibLoadableView {
        let nib = UINib(nibName: T.defaultNibName, bundle: nil)
        register(nib, forCellReuseIdentifier: T.defaultReuseIdentifier)
    }

    func dequeueReusableCell<T:UITableViewCell>(for indexPath:IndexPath) -> T where T : ReusableView {
        guard let cell = dequeueReusableCell(withIdentifier: T.defaultReuseIdentifier, for: indexPath) as? T  else {
            fatalError("No cell for deque identifier : \(T.defaultReuseIdentifier)")
        }
        return cell
    }
}

extension NibLoadableView where Self : UIView  {
    static func viewFromNib(named:String?) -> Self {
        guard let view = Bundle.main.loadNibNamed(named ?? Self.defaultNibName, owner: nil, options: nil)?.first as? Self else { fatalError("No view with such nib name") }
        return view
    }
}
