//
//  ViewController.swift
//  LVModalPopupViewController
//
//  Created by LisnicVictor on 01/11/2018.
//  Copyright (c) 2018 LisnicVictor. All rights reserved.
//

import UIKit

struct SomeStuff : Option {
    var title: String
}

var options : [SomeStuff] = [SomeStuff(title: "First"),SomeStuff(title: "Second"),SomeStuff(title: "Third"),SomeStuff(title: "Fourth")]

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : OptionsPickerTableViewCell = tableView.dequeueReusableCell(for: indexPath)
        cell.configure(with: options, initial: options[indexPath.row])
        cell.delegate = self
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
}

extension ViewController : OptionsPickerTableViewCellDelegate {
    func optionsPickerTableViewCell(cell: OptionsPickerTableViewCell, didSelectOption: Option) {
        print("selected \(didSelectOption.title)")
    }
}
