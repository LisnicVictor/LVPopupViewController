//
//  OptionsPickerTableViewCell.swift
//  MeetNShag
//
//  Created by Victor Lisnic on 10/2/17.
//  Copyright © 2017 Vader. All rights reserved.
//

import UIKit
import LVModalPopupViewController

protocol OptionsPickerTableViewCellDelegate : class {
    func optionsPickerTableViewCell(cell:OptionsPickerTableViewCell,didSelectOption:Option)
}

class OptionsPickerTableViewCell: ModalViewControllerPresentingCell, ReusableView {

    @IBOutlet weak var optionLabel: UILabel!
    var option : Option?
    var options : [Option] = []
    weak var delegate : OptionsPickerTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configure(with options:[Option], initial:Option, delegate:OptionsPickerTableViewCellDelegate? = nil, presentingViewController:UIViewController? = nil) {
        self.options = options
        set(initial: initial)
        self.delegate = delegate
        if self.presentingViewController == nil {
            self.presentingViewController = presentingViewController
        }
    }

    func set(initial option:Option) {
        self.option = option
        optionLabel.text = option.title
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            let optionsViewcontroller = OptionsTableModalViewController(nibName: "ModalPopupViewController", bundle: Bundle(for: ModalPopupViewController.self))
            optionsViewcontroller.configure(with: options)
            optionsViewcontroller.delegate = self
            optionsViewcontroller.setExpandingViewPosition(centerY: cellYPosition)
            present(viewController: optionsViewcontroller)
        }
    }
}

extension OptionsPickerTableViewCell : OptionsTableModalViewControllerDelegate {
    func didSelect(option: Option) {
        set(initial: option)
        delegate?.optionsPickerTableViewCell(cell: self, didSelectOption: option)
    }
}
