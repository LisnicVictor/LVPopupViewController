//
//  OptionTableViewCell.swift
//  MeetNShag
//
//  Created by Victor Lisnic on 10/2/17.
//  Copyright © 2017 Vader. All rights reserved.
//

import UIKit

class OptionTableViewCell: UITableViewCell, ReusableView, NibLoadableView {

    @IBOutlet weak var titleLabel: UILabel!

    func configureWith(option:Option) {
        titleLabel.text = option.title
    }
}
