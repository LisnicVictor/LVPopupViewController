//
//  OptionsTableModalViewController.swift
//  MeetNShag
//
//  Created by Victor Lisnic on 10/2/17.
//  Copyright © 2017 Vader. All rights reserved.
//

import UIKit
import LVModalPopupViewController
protocol Option {
    var title : String {get}
}

protocol OptionsTableModalViewControllerDelegate : class {
    func didSelect(option:Option)
}

class OptionsTableModalViewController: ModalPopupViewController {

    lazy var optionsTableView : UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        insert(view: tableView)
        self.optionsTableViewHeightConstraint = tableView.heightAnchor.constraint(equalToConstant: 0)
        self.optionsTableViewHeightConstraint.isActive = true
        return tableView
    }()

    var optionsTableViewHeightConstraint : NSLayoutConstraint!

    weak var delegate : OptionsTableModalViewControllerDelegate?

    var options : [Option] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        optionsTableView.register(OptionTableViewCell.self)
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 3, height: 3)
    }

    func configure(with options:[Option]) {
        let _ = view.backgroundColor
        self.options = options
        optionsTableView.reloadData()
    }

    override func viewWillAppear(_ animated: Bool) {
        optionsTableView.reloadData()
        optionsTableViewHeightConstraint.constant = optionsTableView.contentSize.height
        view.layoutIfNeeded()
    }
}

extension OptionsTableModalViewController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.didSelect(option: options[indexPath.row])
        dismiss(animated: true, completion: nil)
    }
}

extension OptionsTableModalViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : OptionTableViewCell = tableView.dequeueReusableCell(for: indexPath)
        cell.configureWith(option: options[indexPath.row])
        return cell
    }
}
