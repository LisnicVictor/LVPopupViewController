# LVModalPopupViewController

[![CI Status](http://img.shields.io/travis/LisnicVictor/LVModalPopupViewController.svg?style=flat)](https://travis-ci.org/LisnicVictor/LVModalPopupViewController)
[![Version](https://img.shields.io/cocoapods/v/LVModalPopupViewController.svg?style=flat)](http://cocoapods.org/pods/LVModalPopupViewController)
[![License](https://img.shields.io/cocoapods/l/LVModalPopupViewController.svg?style=flat)](http://cocoapods.org/pods/LVModalPopupViewController)
[![Platform](https://img.shields.io/cocoapods/p/LVModalPopupViewController.svg?style=flat)](http://cocoapods.org/pods/LVModalPopupViewController)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

LVModalPopupViewController is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'LVModalPopupViewController'
```

## Author

LisnicVictor, gerasim.kisslin@gmail.com

## License

LVModalPopupViewController is available under the MIT license. See the LICENSE file for more info.
